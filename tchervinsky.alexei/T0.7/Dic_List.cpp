﻿#include <iostream>
#include "Header.h"

int main()
{
	Dic_list<int> lst;
	lst.push_front(5);
	lst.push_front(7);
	lst.push_front(101);

	for (int i = 0; i < lst.GetSize(); i++)
	{
		std::cout << lst[i] << std::endl;
	}

	std::cout << std::endl << "pop_back " << std::endl << std::endl;

	lst.pop_back();

	for (int i = 0; i < lst.GetSize(); i++)
	{
	    std::cout << lst[i] << std::endl;
	}
    return 0;
}